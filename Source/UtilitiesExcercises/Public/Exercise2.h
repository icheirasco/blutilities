// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Exercise2.generated.h"

/**
 * 
 */

UCLASS()
class UTILITIESEXCERCISES_API UExercise2 : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

#if WITH_EDITOR

	UFUNCTION(BlueprintCallable)
	static void ListActors();

#endif
};
