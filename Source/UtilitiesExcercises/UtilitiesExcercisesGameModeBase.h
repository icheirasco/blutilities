// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UtilitiesExcercisesGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UTILITIESEXCERCISES_API AUtilitiesExcercisesGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
