// Fill out your copyright notice in the Description page of Project Settings.


#include "UtilitiesExcercises/Public/Exercise2.h"


#include "AssetToolsModule.h"
#include "AssetTools/Private/AssetTools.h"
#include "HAL/FileManagerGeneric.h"
#include "Kismet/KismetSystemLibrary.h"

#include "Subsystems/EditorActorSubsystem.h"

void UExercise2::ListActors()
{

	TArray<FString> AssetsToExport;
	
	UEditorActorSubsystem* Eas = GEditor->GetEditorSubsystem<UEditorActorSubsystem>();

	const FAssetToolsModule& AssetToolsModule = FModuleManager::GetModuleChecked<FAssetToolsModule>("AssetTools");

	
	if (!Eas)
	{
		UE_LOG(LogTemp, Error, TEXT("Editor Actor Subsystem not found!"));
	}

	TArray<UActorComponent*> Components = Eas->GetAllLevelActorsComponents();
	
	for (const auto ActorComponent : Components)
	{
		// if(ActorComponent->GetClass()->GetName() == "StaticMeshComponent")
		if (const UStaticMeshComponent* SMComponent = Cast<UStaticMeshComponent>(ActorComponent))
		{
			UStaticMesh* SM = SMComponent->GetStaticMesh();
			UE_LOG(LogTemp, Error, TEXT("Triangles: %d"), SM->GetNumTriangles(0));
			UE_LOG(LogTemp, Error, TEXT("Materials: %d"), SM->GetStaticMaterials().Max());
			
			AssetsToExport.Add(SM->GetPathName());
			
		}
	}
	
	
	for (auto Asset : AssetsToExport)
	{
		UE_LOG(LogTemp, Warning, TEXT("%s"), *Asset)
	}


	FFileManagerGeneric FManager = FFileManagerGeneric();
	FManager.DeleteDirectory(*(UKismetSystemLibrary::GetProjectDirectory() + "Saved/Rework"), 0, 1);
	AssetToolsModule.Get().ExportAssets(AssetsToExport, *(UKismetSystemLibrary::GetProjectDirectory() + "Saved/Rework"));
	
	// FJsonObjectConverter::UStructToJsonObject();
	// FJsonSerializer::Serialize();
	// LayoutService::SaveJson()
}
